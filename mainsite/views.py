from django.shortcuts import render, redirect
from django.http import HttpResponse
from mainsite.models import Post
from datetime import datetime
# Create your views here.
#???
# from .forms import UploadModelForm
#???
def index(request):
    form = UploadModelForm()
    context = {
        'form': form
    }
    return render(request, 'photos/index.html', context)

def homepage(request):
    posts = Post.objects.all()
    post_lists = list()
    now = datetime.now()
    return render(request, 'index.html', locals())

def showpost(request, slug):
    try:
        post = Post.objects.get(slug = slug)
        if post != None :
            now =datetime.now()
            return render(request,'post.html',locals())
    except:
        return HttpResponse('Data not found')
    
    for count, post in enumerate(posts):
        post_lists.append("No.{}:".format(str(count)) + str(post)+"<br><hr>")
        post_lists.append("<small>"+str(post.body)+"</small><br><br>")
    return HttpResponse(post_lists)

def sign(request):
    posts = Post.objects.all()
    post_lists = list()
    now = datetime.now()
    return render(request, 'sign_index.html', locals())
